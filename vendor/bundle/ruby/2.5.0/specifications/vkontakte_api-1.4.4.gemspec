# -*- encoding: utf-8 -*-
# stub: vkontakte_api 1.4.4 ruby lib

Gem::Specification.new do |s|
  s.name = "vkontakte_api".freeze
  s.version = "1.4.4"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["Vsevolod Romashov".freeze]
  s.date = "2017-06-20"
  s.description = "A transparent wrapper for VKontakte API. Supports ruby-way naming of API methods (without method lists inside), optional authorization, files uploading, logging and any faraday-supported http adapter of your choice.".freeze
  s.email = ["7@7vn.ru".freeze]
  s.homepage = "http://7even.github.com/vkontakte_api".freeze
  s.licenses = ["MIT".freeze]
  s.required_ruby_version = Gem::Requirement.new(">= 1.9.2".freeze)
  s.rubygems_version = "2.7.6".freeze
  s.summary = "Ruby wrapper for VKontakte API".freeze

  s.installed_by_version = "2.7.6" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<faraday>.freeze, [">= 0.9.0"])
      s.add_runtime_dependency(%q<faraday_middleware>.freeze, [">= 0.9.1"])
      s.add_runtime_dependency(%q<oj>.freeze, [">= 2.12"])
      s.add_runtime_dependency(%q<oauth2>.freeze, [">= 0.8"])
      s.add_runtime_dependency(%q<hashie>.freeze, [">= 2.0"])
      s.add_runtime_dependency(%q<multi_json>.freeze, [">= 1.3"])
      s.add_runtime_dependency(%q<faraday_middleware-multi_json>.freeze, ["~> 0.0.6"])
      s.add_development_dependency(%q<rake>.freeze, [">= 0"])
      s.add_development_dependency(%q<rspec>.freeze, ["~> 3.0"])
      s.add_development_dependency(%q<mechanize>.freeze, [">= 0"])
      s.add_development_dependency(%q<guard-rspec>.freeze, [">= 0"])
      s.add_development_dependency(%q<rb-fsevent>.freeze, ["~> 0.9.1"])
      s.add_development_dependency(%q<terminal-notifier>.freeze, [">= 0"])
      s.add_development_dependency(%q<terminal-notifier-guard>.freeze, [">= 0"])
      s.add_development_dependency(%q<pry>.freeze, [">= 0"])
      s.add_development_dependency(%q<awesome_print>.freeze, [">= 0"])
      s.add_development_dependency(%q<yard>.freeze, [">= 0"])
      s.add_development_dependency(%q<redcarpet>.freeze, [">= 0"])
      s.add_development_dependency(%q<guard-yard>.freeze, [">= 0"])
    else
      s.add_dependency(%q<faraday>.freeze, [">= 0.9.0"])
      s.add_dependency(%q<faraday_middleware>.freeze, [">= 0.9.1"])
      s.add_dependency(%q<oj>.freeze, [">= 2.12"])
      s.add_dependency(%q<oauth2>.freeze, [">= 0.8"])
      s.add_dependency(%q<hashie>.freeze, [">= 2.0"])
      s.add_dependency(%q<multi_json>.freeze, [">= 1.3"])
      s.add_dependency(%q<faraday_middleware-multi_json>.freeze, ["~> 0.0.6"])
      s.add_dependency(%q<rake>.freeze, [">= 0"])
      s.add_dependency(%q<rspec>.freeze, ["~> 3.0"])
      s.add_dependency(%q<mechanize>.freeze, [">= 0"])
      s.add_dependency(%q<guard-rspec>.freeze, [">= 0"])
      s.add_dependency(%q<rb-fsevent>.freeze, ["~> 0.9.1"])
      s.add_dependency(%q<terminal-notifier>.freeze, [">= 0"])
      s.add_dependency(%q<terminal-notifier-guard>.freeze, [">= 0"])
      s.add_dependency(%q<pry>.freeze, [">= 0"])
      s.add_dependency(%q<awesome_print>.freeze, [">= 0"])
      s.add_dependency(%q<yard>.freeze, [">= 0"])
      s.add_dependency(%q<redcarpet>.freeze, [">= 0"])
      s.add_dependency(%q<guard-yard>.freeze, [">= 0"])
    end
  else
    s.add_dependency(%q<faraday>.freeze, [">= 0.9.0"])
    s.add_dependency(%q<faraday_middleware>.freeze, [">= 0.9.1"])
    s.add_dependency(%q<oj>.freeze, [">= 2.12"])
    s.add_dependency(%q<oauth2>.freeze, [">= 0.8"])
    s.add_dependency(%q<hashie>.freeze, [">= 2.0"])
    s.add_dependency(%q<multi_json>.freeze, [">= 1.3"])
    s.add_dependency(%q<faraday_middleware-multi_json>.freeze, ["~> 0.0.6"])
    s.add_dependency(%q<rake>.freeze, [">= 0"])
    s.add_dependency(%q<rspec>.freeze, ["~> 3.0"])
    s.add_dependency(%q<mechanize>.freeze, [">= 0"])
    s.add_dependency(%q<guard-rspec>.freeze, [">= 0"])
    s.add_dependency(%q<rb-fsevent>.freeze, ["~> 0.9.1"])
    s.add_dependency(%q<terminal-notifier>.freeze, [">= 0"])
    s.add_dependency(%q<terminal-notifier-guard>.freeze, [">= 0"])
    s.add_dependency(%q<pry>.freeze, [">= 0"])
    s.add_dependency(%q<awesome_print>.freeze, [">= 0"])
    s.add_dependency(%q<yard>.freeze, [">= 0"])
    s.add_dependency(%q<redcarpet>.freeze, [">= 0"])
    s.add_dependency(%q<guard-yard>.freeze, [">= 0"])
  end
end
