class BanUser
  def self.ban(body, id)
    string = body[5...body.length]
    puts(string)
    # Super Admin (Ban everyone)
    if $db[id][:status] == 3
      if $db.key?(string) && $db[string][:isban]
        $vk.messages.send(peer_id: id, message: 'Пользователь уже забанен.')
      end
      if $db.key?(string) && $db[string][:isban] == false
        $db[string][:isban] = true
        $vk.messages.send(peer_id: id, message: 'Пользователь успешно забанен.')
      end
      unless $db.key?(string)
        $vk.messages.send(peer_id: id, message: 'Пользователь никогда не писал боту.')
      end
    end
    # Classic Bans
    if $db[id][:status] == 2
      if $db.key?(string) && $db[string][:status] == 0
        if $db.key?(string) && $db[string][:isban]
          $vk.messages.send(peer_id: id, message: 'Пользователь уже забанен.')
        end
        if $db.key?(string) && $db[string][:isban] == false
          $db[string][:isban] = true
          $vk.messages.send(peer_id: id, message: 'Пользователь успешно забанен.')
        end
        unless $db.key?(string)
          $vk.messages.send(peer_id: id, message: 'Пользователь никогда не писал боту.')
        end
      end
      if $db.key?(string) && $db[string][:status] == 1
        $vk.messages.send(peer_id: id, message: 'Нельзя забанить VIP пользователя.')
      end
      if $db.key?(string) && $db[string][:status] == 2
        $vk.messages.send(peer_id: id, message: 'Нельзя забанить администратора.')
      end
      if $db.key?(string) && $db[string][:status] == 3
        $vk.messages.send(peer_id: id, message: 'Нельзя забанить суперадминистратора.')
      end
    end
  end

  def self.unban(body, id)
    string = body[7...body.length]
    if $db.key?(string) && $db[string][:isban] == false
      $vk.messages.send(peer_id: id, message: 'Пользователь не забанен.')
    end
    if $db.key?(string) && $db[string][:isban] == true
      $db[string][:isban] = false
      $vk.messages.send(peer_id: id, message: 'Пользователь успешно разбанен.')
    end
    unless $db.key?(string)
      $vk.messages.send(peer_id: id, message: 'Пользователь никогда не писал боту.')
    end
  end

  def self.isban(banstatus)
    return 'Да' if banstatus
    return 'Нет' unless banstatus
  end
end
