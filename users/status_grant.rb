class StatusGrant
  def self.off_ten_limits(body, id)
    string = body[14...body.length]
    if $db.key?(string)
      $db[string][:extraten] = false
      $vk.messages.send(peer_id: id, message: 'Пользователь лишен 10-ти проверок. Их не будет завтра.')
    end
    unless $db.key?(string)
      $vk.messages.send(peer_id: id, message: 'Пользователь никогда не писал боту.')
    end
  end

  def self.ten_limits(body, id)
    string = body[11...body.length]
    if $db.key?(string)
      $db[string][:limits] = $db[string][:limits] + 10
      $db[string][:extraten] = true
      $vk.messages.send(peer_id: id, message: 'Пользователю успешно выдано 10 дополнительных проверок.')
    end
    unless $db.key?(string)
      $vk.messages.send(peer_id: id, message: 'Пользователь никогда не писал боту.')
    end
  end

  def self.admin(body, id)
    string = body[7...body.length]
    if $db.key?(string) && $db[string][:status] != 2
      $db[string][:status] = 2
      $db[string][:limits] = 20
      $db[string][:masslimits] = 3
      $db[string][:era] = 8_141_695_688
      $vk.messages.send(peer_id: id, message: 'Пользователь успешно добавлен в администраторы.')
    end
    if $db.key?(string) && $db[string][:status] == 2
      $db[string][:limits] = 20
      $db[string][:masslimits] = 3
      $db[string][:era] = 8_141_695_688
      $vk.messages.send(peer_id: id, message: 'Лимиты пользователя сброшены.')
    end
    unless $db.key?(string)
      $vk.messages.send(peer_id: id, message: 'Пользователь никогда не писал боту.')
    end
  end

  def self.adminweek(body, id)
    string = body[11...body.length]
    if $db.key?(string) && $db[string][:status] != 2
      $db[string][:status] = 2
      $db[string][:masslimits] = 3
      $db[id][:limits] = 20
      t = Time.now.to_i
      t += 604_800
      $db[id][:era] = t
      $vk.messages.send(peer_id: id, message: 'Пользователь успешно добавлен в администраторы.')
    end
    if $db.key?(string) && $db[string][:status] == 2
      $db[string][:masslimits] = 3
      $db[string][:limits] = 20
      t = Time.now.to_i
      t += 604_800
      $db[string][:era] = t
      $vk.messages.send(peer_id: id, message: 'Лимиты и время пользователя сброшены.')
    end
    unless $db.key?(string)
      $vk.messages.send(peer_id: id, message: 'Пользователь никогда не писал боту.')
    end
  end

  def self.superadmin(body, id)
    string = body[12...body.length]
    if $db.key?(string) && $db[string][:status] != 3
      $db[string][:status] = 3
      $db[string][:limits] = 9999
      $db[string][:masslimits] = 9999
      $db[string][:era] = 8_141_695_688
      $vk.messages.send(peer_id: id, message: 'Пользователь успешно добавлен в суперадмины.')
    end
    if $db.key?(string) && $db[string][:status] == 3
      $db[string][:limits] = 9999
      $db[string][:masslimits] = 9999
      $db[string][:era] = 8_141_695_688
      $vk.messages.send(peer_id: id, message: 'Лимиты пользователя сброшены.')
    end
    unless $db.key?(string)
      $vk.messages.send(peer_id: id, message: 'Пользователь никогда не писал боту.')
    end
  end

  def self.vipweek(body, id)
    string = body[9...body.length]
    if $db.key?(string) && $db[string][:status] != 1
      $db[string][:status] = 1
      $db[string][:limits] = 10
      $db[string][:masslimits] = 1
      t = Time.now.to_i
      t += 604_800
      $db[id][:era] = t
      $vk.messages.send(peer_id: id, message: 'Пользователь успешно добавлен в VIP.')
    end
    if $db.key?(string) && $db[string][:status] == 1
      $db[string][:limits] = 10
      $db[string][:masslimits] = 1
      t = Time.now.to_i
      t += 604_800
      $db[id][:era] = t
      $vk.messages.send(peer_id: id, message: 'Лимиты и время пользователя сброшены.')
    end
    unless $db.key?(string)
      $vk.messages.send(peer_id: id, message: 'Пользователь никогда не писал боту.')
    end
  end

  def self.vip(body, id)
    string = body[5...body.length]
    if $db.key?(string) && $db[string][:status] != 1
      $db[string][:status] = 1
      $db[string][:limits] = 10
      $db[string][:masslimits] = 1
      $db[string][:era] = 8_141_695_688
      $vk.messages.send(peer_id: id, message: 'Пользователь успешно добавлен в VIP.')
    end
    if $db.key?(string) && $db[string][:status] == 1
      $db[string][:limits] = 10
      $db[string][:masslimits] = 1
      $db[string][:era] = 8_141_695_688
      $vk.messages.send(peer_id: id, message: 'Лимиты пользователя сброшены.')
    end
    unless $db.key?(string)
      $vk.messages.send(peer_id: id, message: 'Пользователь никогда не писал боту.')
    end
  end

  def self.vipmonth(body, id)
    string = body[10...body.length]
    if $db.key?(string) && $db[string][:status] != 1
      $db[string][:status] = 1
      $db[string][:limits] = 10
      $db[string][:masslimits] = 1
      t = Time.now.to_i
      t += 2_592_000
      $db[string][:era] = t
      $vk.messages.send(peer_id: id, message: 'Пользователь успешно добавлен в VIP.')
    end
    if $db.key?(string) && $db[string][:status] == 1
      $db[string][:limits] = 10
      $db[string][:masslimits] = 1
      t = Time.now.to_i
      t += 2_592_000
      $db[string][:era] = t
      $vk.messages.send(peer_id: id, message: 'Лимиты и время пользователя сброшены.')
    end
    unless $db.key?(string)
      $vk.messages.send(peer_id: id, message: 'Пользователь никогда не писал боту.')
    end
  end

  def self.user(body, id)
    string = body[6...body.length]
    if $db.key?(string) && $db[string][:status] != 0
      $db[string][:status] = 0
      $db[string][:limits] = 1
      $db[string][:era] = 8_141_695_688
      $vk.messages.send(peer_id: id, message: 'Пользователь успешно добавлен в юзеры.')
    end
    if $db.key?(string) && $db[string][:status] == 0
      $db[string][:limits] = 1
      $db[string][:era] = 8_141_695_688
      $vk.messages.send(peer_id: id, message: 'Лимиты пользователя сброшены.')
    end
    unless $db.key?(string)
      $vk.messages.send(peer_id: id, message: 'Пользователь никогда не писал боту.')
    end
  end

  def self.status(body, id)
    string = body[8...body.length]
    if $db.key?(string)
      type = ConfigData.type_of_user($db[string][:status]).to_s
      banstatus = BanUser.isban($db[string][:isban]).to_s
      time = Time.at($db[string][:era])
      $vk.messages.send(peer_id: id, message: 'Статус пользователя: ' + type + "\n" + 'У него осталось ' + $db[string][:limits].to_s + " обычных проверок.\n" + 'У него осталось: ' + $db[id][:masslimits].to_s + " массовых проверок.\n" + 'Забанен? ' + banstatus + "\nКогда конец подписки? " + time.to_s)
    end
    unless $db.key?(string)
      $vk.messages.send(peer_id: id, message: 'Пользователь никогда не писал боту.')
    end
  end
end
