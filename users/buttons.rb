require 'json'

class Buttons
  def self.get_button(label, color, payload = '')
    {
      "action": {
        "type": 'text',
        "payload": JSON.dump(payload),
        "label": label
      },
      "color": color
    }
  end

  def self.keyboard1
    @keyboard = {
      "one_time": true,
      "buttons": [

        [get_button('Начать', 'primary')]
      ]
    }
    @keyboard = JSON.dump(@keyboard).force_encoding('UTF-8')
  end

  def self.keyboard2
    @keyboard = {
      "one_time": true,
      "buttons": [

        [get_button('Ввести почту', 'positive')],
        [get_button('Ввести minecraft', 'positive')],
        [get_button('Ограничить доступ к моей почте', 'primary')],
        [get_button('Статус', 'default')],
        [get_button('Закрыть', 'negative')]

      ]
    }
    @keyboard = JSON.dump(@keyboard).force_encoding('UTF-8')
  end
end
