require_relative '../users/status_grant'
require_relative '../bot/check'
require_relative '../bot/email_block'
require_relative '../users/ban_user'
class Commands
  def self.commands(body, id)
    if body == '!admins' && ($db[id][:status] == 3)
      message = ''
      (0...$db.keys.length).each do |x|
        user = $db.keys[x]
        if $db[user][:status] == 2
          message = message + "\n@id" + $db.keys[x].to_s
        end
      end
      unless message == ''
        $vk.messages.send(peer_id: id, message: message)
        return
      end
      if message == ''
        $vk.messages.send(peer_id: id, message: 'Нет активных Админов')
        return
      end
    end
    if body == '!vips' && ($db[id][:status] == 3)
      message = ''
      (0...$db.keys.length).each do |x|
        user = $db.keys[x]
        if $db[user][:status] == 1
          message = message + "\n@id" + $db.keys[x].to_s
        end
      end
      unless message == ''
        $vk.messages.send(peer_id: id, message: message)
        return
      end
      if message == ''
        $vk.messages.send(peer_id: id, message: 'Нет активных VIP пользователей')
        return
      end
    end
    if body == '!superadmins' && ($db[id][:status] == 3)
      message = ''
      (0...$db.keys.length).each do |x|
        user = $db.keys[x]
        if $db[user][:status] == 3
          message = message + "\n@id" + $db.keys[x].to_s
        end
      end
      unless message == ''
        $vk.messages.send(peer_id: id, message: message)
        return
      end
      if message == ''
        $vk.messages.send(peer_id: id, message: 'Нет активных СуперАдминов')
        return
      end
    end
    if body == '!users' && ($db[id][:status] == 3 || $db[id][:status] == 2 || $db[id][:status] == 1)
      message = ''
      (0...$db.keys.length).each do |x|
        user = $db.keys[x]
        if ($db[user][:status]).zero?
          message = message + "\n@id" + $db.keys[x].to_s
        end
      end
      unless message == ''
        $vk.messages.send(peer_id: id, message: message)
        return
      end
      if message == ''
        $vk.messages.send(peer_id: id, message: 'Нет активных пользователей')
        return
      end
    end
    if body.start_with?('!ban') && ($db[id][:status] == 3 || $db[id][:status] == 2)
      BanUser.ban(body, id)
    end
    if body.start_with?('!unban') && ($db[id][:status] == 3 || $db[id][:status] == 2)
      BanUser.unban(body, id)
    end
    if body.start_with?('!mine') && ($db[id][:status] == 0 || $db[id][:status] == 1 || $db[id][:status] == 2 || $db[id][:status] == 3)
      if ($db[id][:limits]).zero?
        $vk.messages.send(peer_id: id, message: 'У Вас закончились проверки.')
      end
      Check.minecraft(body.downcase, id) unless ($db[id][:limits]).zero?
    end
    if $boolmine && ($db[id][:limits] > 0)
      $db[id][:limits] -= 1 if $db.key?(id)
    end
    if body.start_with?('!weekadmin') && ($db[id][:status] == 3)
      StatusGrant.adminweek(body, id)
    end
    if body.start_with?('!admin') && ($db[id][:status] == 3)
      StatusGrant.admin(body, id)
    end
    if body.start_with?('!superadmin') && ($db[id][:status] == 3)
      StatusGrant.superadmin(body, id)
    end
    if body.start_with?('!weekvip') && ($db[id][:status] == 3)
      StatusGrant.vipweek(body, id)
    end
    if body.start_with?('!monthvip') && ($db[id][:status] == 3)
      StatusGrant.vipmonth(body, id)
    end
    if body.start_with?('!vip') && ($db[id][:status] == 3)
      StatusGrant.vip(body, id)
    end
    if body.start_with?('!user') && ($db[id][:status] == 3)
      StatusGrant.user(body, id)
    end
    if body.start_with?('!tenlimits') && ($db[id][:status] == 3)
      StatusGrant.ten_limits(body, id)
    end
    if body.start_with?('!offtenlimits') && ($db[id][:status] == 3)
      StatusGrant.off_ten_limits(body, id)
    end
    if body.start_with?('!block') && ($db[id][:status] == 3 || $db[id][:status] == 2 || $db[id][:status] == 1)
      EmailBlock.email_block(body, id)
    end
    if body.start_with?('!unblock') && ($db[id][:status] == 3 || $db[id][:status] == 2 || $db[id][:status] == 1)
      EmailBlock.email_unblock(body, id)
    end
    if body.start_with?('!status') && ($db[id][:status] == 3)
      StatusGrant.status(body, id)
    end
    if body.start_with?('!mass')
      if ($db[id][:masslimits]).zero?
        $vk.messages.send(peer_id: id, message: 'У Вас закончились массовые проверки на сегодня.')
      end
      Check.mass(body.downcase, id) unless ($db[id][:masslimits]).zero?
    end
    if $boolmass && ($db[id][:masslimits] > 0)
      $db[id][:masslimits] -= 1 if $db.key?(id)
    end
    if body.downcase == 'статус'
      type = ConfigData.type_of_user($db[id][:status]).to_s
      banstatus = BanUser.isban($db[id][:isban]).to_s
      time = Time.at($db[id][:era])
      commands = ''
      if $db[id][:status] == 0
        commands = "\n1) !mine - Проверка аккаунта Minecraft в базе."
      end
      if $db[id][:status] == 1
        commands = "\n1) !block - Блок почты для проверки.\n2) !unblock - Снятие блока почты для проверки.\n3) Статус - Информация о пользователе.\n4) !mass - Массовая проверка по всем почтам.\n5) !users - Список обычных пользователей.\n6) !mine - Проверка аккаунта Minecraft в базе."
      end
      if $db[id][:status] == 2
        commands = "\n1) !ban - Бан пользователя на проверки.\n2) !unban - Разбан пользователя на проверки.3) !block - Блок почты для проверки.\n4) !unblock - Снятие блока почты для проверки.\n5) Статус - Информация о пользователе.\n6) !mass - Массовая проверка по всем почтам.\n7) !users - Список обычных пользователей.\n8) !vips - Список VIP пользователей.\n9) !mine - Проверка аккаунта Minecraft в базе."
      end
      if $db[id][:status] == 3
        commands = "\n1) !ban - Бан пользователя на проверки.\n2) !unban - Разбан пользователя на проверки.\n3) !weekadmin - Админ-доступ на неделю.\n4) !admin - Админ-доступ навсегда.\n5) !superadmin - Супер-админка.\n6) !weekvip - Вип-доступ на неделю.\n7) !monthvip - Вип-доступ на месяц.\n8) !vip - Вип-доступ навсегда.\n9) !user - Доступ обычного пользователя навсегда.\n10) !block - Блок почты для проверки.\n11) !unblock - Снятие блока почты для проверки.\n12) !offtenlimits - Убрать 10 дополнительных проверок (на следующие сутки).\n13) Статус - Информация о пользователе.\n14) !mass - Массовая проверка по всем почтам.\n15) !tenlimits - Дополнительно 10 проверок.\n16) !admins - Список Админов.\n17) !vips - Список VIP.\n18) !superadmins - Список СуперАдминов.\n19) !users - Список обычных пользователей.\n20) !status - Информация о конкретном пользователе.\n21) !mine - Проверка аккаунта Minecraft в базе."
      end
      $vk.messages.send(peer_id: id, message: 'Твой статус: ' + type + "\n" + 'У тебя осталось ' + $db[id][:limits].to_s + " обычных проверок E-mail/Minecraft.\n" + 'У тебя осталось: ' + $db[id][:masslimits].to_s + " массовых проверок.\n" + 'Забанен? ' + banstatus + "\nКогда конец подписки? " + time.to_s + commands)
    end
  end
end