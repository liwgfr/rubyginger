
class ButtonsConversation
  def self.buttons_replies(body, id)
    @body = body.downcase
    @id = id
    $vk.messages.send(peer_id: @id, message: 'Выберите кнопку!', keyboard: Buttons.keyboard2) if @body == 'начать'
    $vk.messages.send(peer_id: @id, message: "Мне очень важен feedback от Вас. Если Вам не трудно, напишите свои пожелания или жалобы мне в ВК\n@liwgfr") if @body == 'закрыть'
    $vk.messages.send(peer_id: @id, message: "Напишите !block и вашу почту (Например, !block test@test.ru). Доступно только для админов и VIP! По всем вопросам пишите @liwgfr") if @body == 'ограничить доступ к моей почте'
    $vk.messages.send(peer_id: @id, message: 'Напишите почту. Вы также можете присылать почты без использования кнопок!') if @body == 'ввести почту'
    $vk.messages.send(peer_id: @id, message: 'Напишите !mine и Minecraft аккаунт для проверки.') if @body == 'ввести minecraft'
  end
end
