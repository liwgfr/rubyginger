class EmailValidation
  def self.email_check(body)
    /\A([\w+\-].?)+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i.match?(body)
  end
end