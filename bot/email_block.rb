class EmailBlock
  def self.email_block(body, id)
    string = body.downcase[7...body.length]
    if $email_db.key?(string)
      $vk.messages.send(peer_id: id, message: 'Почта уже недоступна для проверки.')
    end
    unless $email_db.key?(string)
      $email_db[string] = 'ban'
      $vk.messages.send(peer_id: id, message: 'Теперь эту почту нельзя проверять.')
    end
  end
  def self.email_unblock(body, id)
    string = body.downcase[9...body.length]
    unless $email_db.key?(string)
      $vk.messages.send(peer_id: id, message: 'Почта уже доступна для проверки.')
    end
    if $email_db.key?(string)
      $email_db.delete(string)
      $vk.messages.send(peer_id: id, message: 'Теперь эту почту можно проверять.')
    end
  end
end