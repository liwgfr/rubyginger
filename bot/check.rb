require_relative '../init/config_data'
require 'json'
require 'vkontakte_api'
class Check
  def self.minecraft(body, id)

    @body = body.downcase[6...body.length]
    url = "https://leakcheck.net/api/?key=#{ConfigData.access_key}&check=#{@body}&type=mc"
    uri = URI(url)
    response = Net::HTTP.get(uri)
    data = eval(response)

    if data[:success]
      found = data[:found]
      if found <= 10
        cnt = 0
        message = ''
        while cnt < found
          message = message + "\n" + data[:result][cnt]
          cnt += 1
        end
        $vk.messages.send(peer_id: id, message: 'Найдено ' + found.to_s + ' результатов. В файл они загружаться не будут.')
        $vk.messages.send(peer_id: id, message: message)
      end
      if found > 10
        cnt = 0
        message = 'Создатель бота: https://vk.com/id453785318: '
        while cnt < found
          message = message + "\n" + data[:result][cnt]
          cnt += 1
        end
        File.open('results.txt', 'w') { |file| file.write message }
        upload_url = $vk.docs.getMessagesUploadServer(type: 'doc', peer_id: id)[:upload_url]
        file = VkontakteApi.upload(url: upload_url, file: ['./results.txt', 'multipart/form-data'])[:file]
        a = $vk.docs.save(file: file, title: 'results_' + body + '.txt')
        owner_id = a[0][:owner_id]
        doc_id = a[0][:id]
        $vk.messages.send(peer_id: id, message: 'Найдено больше 10-ти результатов. Они загружены в файл', attachment: 'doc' + owner_id.to_s + '_' + doc_id.to_s)
      end
      $boolmine = true
    end
    unless data[:success]
      $vk.messages.send(peer_id: id, message: 'Такого аккаунта в нашей базе на данный момент нет.') if data[:error] == 'Not found'
      $vk.messages.send(peer_id: id, message: 'Лимиты проверок исчерпаны.') if data[:error] == 'Limit reached"'
      $vk.messages.send(peer_id: id, message: 'Подождите ~15 секунд, возможно кто-то использует бота параллельно с Вами. В скором времени данные ограничения будут сняты только для Admin пользователей.') if data[:error] == 'Wait 15 seconds before the next check'
      $boolmine = false
    end
  end





  def self.email(body, id)
    @body = body.downcase
    url = "https://leakcheck.net/api/?key=#{ConfigData.access_key}&check=#{@body}&type=email"
    uri = URI(url)
    response = Net::HTTP.get(uri)
    data = eval(response)
    if data[:success]
      found = data[:found]
      if found <= 10
        cnt = 0
        message = ''
        while cnt < found
          message = message + "\n" + data[:result][cnt][:line]
          cnt += 1
        end
        $vk.messages.send(peer_id: id, message: 'Найдено ' + found.to_s + ' результатов. В файл они загружаться не будут.')
        $vk.messages.send(peer_id: id, message: message)
      end
      if found > 10
        cnt = 0
        message = 'Создатель бота: https://vk.com/id453785318: '
        while cnt < found
          message = message + "\n" + data[:result][cnt][:line]
          cnt += 1
        end
        File.open('results.txt', 'w') { |file| file.write message }
        upload_url = $vk.docs.getMessagesUploadServer(type: 'doc', peer_id: id)[:upload_url]
        file = VkontakteApi.upload(url: upload_url, file: ['./results.txt', 'multipart/form-data'])[:file]
        a = $vk.docs.save(file: file, title: 'results_' + body + '.txt')
        owner_id = a[0][:owner_id]
        doc_id = a[0][:id]
        $vk.messages.send(peer_id: id, message: 'Найдено больше 10-ти результатов. Они загружены в файл', attachment: 'doc' + owner_id.to_s + '_' + doc_id.to_s)
      end
      $bool = true
    end
    unless data[:success]
      $vk.messages.send(peer_id: id, message: 'Такой почты в нашей базе на данный момент нет.') if data[:error] == 'Not found'
      $vk.messages.send(peer_id: id, message: 'Лимиты проверок на сегодня исчерпаны.') if data[:error] == 'Limit reached"'
      $vk.messages.send(peer_id: id, message: 'Подождите ~15 секунд, возможно кто-то использует бота параллельно с Вами. В скором времени данные ограничения будут сняты только для Admin пользователей.') if data[:error] == 'Wait 15 seconds before the next check'
      $bool = false
    end
  end
  def self.mass(body, id)
    @body = body.downcase[6...body.length]
    url = "https://leakcheck.net/api/?key=#{ConfigData.access_key}&check=#{@body}&type=mass"
    uri = URI(url)
    response = Net::HTTP.get(uri)
    data = eval(response)
    if data[:success]
      found = data[:found]
      if found <= 10
        cnt = 0
        message = ''
        while cnt < found
          message = message + "\n" + data[:result][cnt][:line]
          cnt += 1
        end
        $vk.messages.send(peer_id: id, message: 'Найдено ' + found.to_s + ' результатов. В файл они загружаться не будут.')
        $vk.messages.send(peer_id: id, message: message)
      end
      if found > 10
        cnt = 0
        message = 'Создатель проекта https://vk.com/id453785318: '
        while cnt < found
          message = message + "\n" + data[:result][cnt][:line]
          cnt += 1
        end
        File.open('results.txt', 'w') { |file| file.write message }
        upload_url = $vk.docs.getMessagesUploadServer(type: 'doc', peer_id: id)[:upload_url]
        file = VkontakteApi.upload(url: upload_url, file: ['./results.txt', 'multipart/form-data'])[:file]
        a = $vk.docs.save(file: file, title: 'results_' + body + '.txt')
        owner_id = a[0][:owner_id]
        doc_id = a[0][:id]
        $vk.messages.send(peer_id: id, message: 'Найдено больше 10-ти результатов. Они загружены в файл', attachment: 'doc' + owner_id.to_s + '_' + doc_id.to_s)
      end
      $boolmass = true
    end
    unless data[:success]
      $vk.messages.send(peer_id: id, message: 'Такой почты в нашей базе на данный момент нет.') if data[:error] == 'Not found'
      $vk.messages.send(peer_id: id, message: 'Лимиты проверок на сегодня исчерпаны.') if data[:error] == 'Limit reached"'
      $vk.messages.send(peer_id: id, message: 'Подождите ~15 секунд, возможно кто-то использует бота параллельно с Вами. В скором времени данные ограничения будут сняты только для Admin пользователей.') if data[:error] == 'Wait 15 seconds before the next check'
      $boolmass = false
    end
  end
end
