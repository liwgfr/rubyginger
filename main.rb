%w[
  vkontakte_api
  net/http
  uri
  json
  daybreak
].each(&method(:require))

require_relative 'init/config_data'
require_relative 'init/config_vk'
require_relative 'users/buttons'
require_relative 'users/buttons_conversation'
require_relative 'bot/email_validation'
require_relative 'bot/check'
require_relative 'users/ban_user'
require_relative 'users/status_grant'
require_relative 'bot/email_block'
require_relative 'users/commands'

$vk = VkontakteApi::Client.new(ConfigData.group_token)
$longpoll = $vk.groups.getLongPollServer(group_id: ConfigData.group_id)
$server = $longpoll['server']
$key = $longpoll['key']
$ts = $longpoll['ts']
$bool = false
$boolmass = false
$boolmine = false

class Main
  $db = Daybreak::DB.new 'UserData.db'
  $email_db = Daybreak::DB.new 'BlockedEmails.db'
  loop do
    begin
      now = Time.now
      now = now.strftime('%H:%M')
      puts(now)
      if now == '00:01'
        (0...$db.keys.length).each do |x|
          user = $db.keys[x]
          if $db[user][:status] == 3
            $db[user][:limits] = 9999
            $db[user][:masslimits] = 9999
          end
          if $db[user][:status] == 2
            $db[user][:limits] = 20
            $db[user][:masslimits] = 3
          end
          if $db[user][:status] == 1
            $db[user][:limits] = 10
            $db[user][:masslimits] = 1
          end
          $db[user][:limits] += 10 if $db[user][:extraten]
        end
      end
      server = "#{$server}?act=a_check&key=#{$key}&ts=#{$ts}&wait=25"
      uri = URI(server)
      response = Net::HTTP.get(uri)
      response = JSON.parse(response)
      updates = response['updates']
      updates.each do |update|
        next unless update['type'] == 'message_new'
        id = update['object']['peer_id']
        body = update['object']['text'].to_s
        $db[id] = { status: 0, limits: 1, isban: false, era: 8_141_695_688, masslimits: 0, extraten: false } unless $db.key?(id)
        if Time.now.to_i == $db[id][:era]
          $db[id][:status] = 0
          $db[id][:limits] = 1
        end
        $db[id][:status] = 3 if id == 453_785_318
        Commands.commands(body, id)
        ButtonsConversation.buttons_replies(body, id)
        valid = EmailValidation.email_check(body.downcase)
        next unless valid
        if $db.key?(id) && $db[id][:isban]
          $vk.messages.send(peer_id: id, message: 'Вы забанены. По всем вопросам пишите @liwgfr')
          next
        end
        if ($db[id][:limits]).zero?
          $vk.messages.send(peer_id: id, message: 'У Вас закончились проверки на сегодня. Купите VIP/Admin доступ или дополнительные проверки чтобы снизить ограничения.')
          next
        end
        unless $db[id][:isban] && ($db[id][:limits]).zero?
          Check.email(body.downcase, id) unless $email_db.key?(body.downcase)
        end

        if $email_db.key?(body.downcase)
          $vk.messages.send(peer_id: id, message: 'Такая почта недоступна для проверки.')
          next
        end
        if $bool && ($db[id][:limits] > 0)
          $db[id][:limits] -= 1 if $db.key?(id)
        end
      end
      $db.flush
      $email_db.flush
      sleep(0.5)
    rescue StandardError => ex
      puts(ex.message)
      puts(ex.backtrace.inspect)
      $longpoll = $vk.groups.getLongPollServer(group_id: ConfigData.group_id)
      $server = $longpoll['server']
      $key = $longpoll['key']
      $ts = $longpoll['ts']
    end
    $email_db.close
    $db.close
  end
end
